<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Qoo10 IMEI Scanner</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
</head>
<style>
    .alert {
        font-size: 16px;
        font-weight: 600;
    }

    form {
        margin: 0 auto;
    }
</style>
<body>
    <div class="container">
        <div class="row" style="margin-bottom: 7%">&nbsp;</div>
        <div class="row">
            <div class="col-md-3">&nbsp;</div>
            <div class="col-md-6">
                <div class="alert alert-success" id="success" role="alert" style="text-align: center;display: none">Record Stored</div>
                <div class="alert alert-danger" id= "fail" role="alert" style="text-align: center;display: none">Failed : Contact Admin With Order Number</div>
            </div>
            <div class="col-md-3">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-3">&nbsp;</div>
            <div class="col-md-6">
                <h1 style="text-align: center">Qoo10 IMEI Scanner</h1>
            </div>
            <div class="col-md-3">&nbsp;</div>
        </div>
        <div class="row">&nbsp;</div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6" style="text-align: center">
                <form method="post" id="imei_form">
                    <div class="form-group main">
                        <button type="button" id="add" style="width: 100%;height: 80px;font-size: 25px;" class="btn btn-warning">Add More IMEI</button>
                        <h3 for="scanned_order_id">BTFL Order ID</h3>
                        <input style="width:100%;float:left;margin-bottom: 25px;height: 100px;font-size: 50px" type="text" name="scanned_order_id" placeholder="BTFL Order Number" required class="form-control" id="scanned_order_id"/>
                        <h3 for="channel_order_id">Channel Order ID</h3>
                        <input style="width:100%;float:left;margin-bottom: 25px;height: 100px;font-size: 50px" type="text" name="channel_order_id" placeholder="Channel Order Number" required class="form-control" id="channel_order_id"/>
                        <h3 for="scanned_order_id">Scan IMEI Number</h3>
                        <input style="width:100%;float:left;margin-bottom: 25px;height: 100px;font-size: 50px" type="text" name="imei_no[]" placeholder="IMEI Number" required class="form-control imei_item" id="origin_imei"/>
                    </div>
                    <button style="height: 80px; width: 250px; font-size: 23px" type="submit" id="submit" class="btn btn-primary">Save Data</button>
                </form>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</body>
<script>

    $(document).ready(function(){
        $('#channel_order_id').focus();

        $( "#imei_form" ).on( "submit", function(e) {
            e.preventDefault();

            var imei_data = '';

            var channel_order_id = $('#channel_order_id').val();
            var scanned_order_id = $('#scanned_order_id').val();

            var url = 'https://sync.synagie.com/btfl_qoo10_imei_scanner/qoo10_imei.php?channel_order_id='+ channel_order_id +'&scanned_order_id=' + scanned_order_id;

            var imei_count = $('.imei_item');

            for (var i = 0; i < imei_count.length; i++) {
                imei_data += '&imei[]=' + $(imei_count[i]).val();
            };

            url += imei_data;

            console.log(url);

            $.ajax({
                url: url,
                method: "GET",
                dataType: "json",
                success: function(result) {
                    $('#fail').css("display",  "none");
                    $('#success').css("display",  "block");
                    $(window).scrollTop(0);
                    $('.extra').remove();
                    $('#channel_order_id').val('');
                    $('#scanned_order_id').val('');
                    $('#origin_imei').val('');
                },
                error: function(result) {
                    $('#success').css("display",  "none");
                    $('#fail').css("display",  "block");
                    $(window).scrollTop(0);
                }
            })
        });
    });

    $('#minus').click(function(){
        if(parseInt($('#number').val()) > 1){
            $('#number').val(parseInt($('#number').val()) - 1);
            $('#scanned_order_id').focus();
        }
    });

    $('#add').click(function(e){
        e.preventDefault();
        $('.main').append('<div class="input-group extra" style="margin-bottom: 25px"><input style="height:100px;font-size:50px;width:100%;float:left;" type="text" name="imei_no[]" placeholder="IMEI Number" required class="form-control imei_item"/><span class="input-group-addon"><a href="#" class="del-row">Remove</a></span></div>');
        $('.del-row').click(function(e){
            e.preventDefault();
            console.log('Working');
            ($(this).parent().parent()).remove();
        })
    });
</script>
</html>