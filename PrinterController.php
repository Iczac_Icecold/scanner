<?php
if ($_POST) {
    if ($_POST['name']){
//        All in One LOL
        if($_POST['name'] == 'gprinter') { // G Printer
            file_put_contents('print.bat', 'SumatraPDF.exe -print-to "Gprinter  GP-1324D" -silent "dump\%1.pdf"');
            echo json_encode(array("status" => "okay"));
        } else if ($_POST['name'] == 'zprinter') {  // Z Printer
            file_put_contents('print.bat', 'SumatraPDF.exe -print-to "ZDesigner ZT410-203dpi ZPL" -silent "dump\%1.pdf"');
            echo json_encode(array("status" => "okay"));
        } else if ($_POST['name'] == 'sfprinter') { // SF Printer
            file_put_contents('print.bat', 'SumatraPDF.exe -print-to "ZDesigner GK888t (EPL)" -silent "dump\%1.pdf"');
            echo json_encode(array("status" => "okay"));
        }  else if ($_POST['name'] == 'init') { // Init Route to Check Current Printer
            $result = file_get_contents('print.bat');
            echo json_encode(array("status" => $result));
        } else if ($_POST['name'] == 'sync') {  // Update or Sync
            shell_exec('"C:\Program Files\Git\bin\git" pull');
            echo json_encode(array("status" => "okay"));
        } else if ($_POST['name'] == 'repair') {    // Repair the System
            shell_exec('"C:\Program Files\Git\bin\git" fetch -all');
            shell_exec('"C:\Program Files\Git\bin\git" reset --hard origin/master');
            echo json_encode(array("status" => "okay"));
        } else {    // Error Route
            echo json_encode(array("status" => "error"));
        }
    }
}

?>