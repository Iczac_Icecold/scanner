<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<title>Lazada IMEI Scanner</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style>
html{
    text-align:center;
}
body{
    text-align:center;
}
form{
    max-width:500px;
    margin:0 auto;


}
form input{
    font-size:50px;
}
.form-control{
    height:auto;
    font-size:50px;
}
#number{
    pointer-events:none;
}
#wrap a{
    display:block;
    font-size:30px;
    display: block;
    border-radius: 25px;
    font-size: 15px;
    padding: 10px;
    border: 1px solid gray;
}
</style>

    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

</head>

<body>
<?php
ini_set("log_errors", 1);
error_reporting(E_ALL); 
ini_set("error_log", "./logs/".date('Y-m-d')."shipment_create.log");

function print_label($url,$scanned_order_id){
    $writeto = dirname(__FILE__) . '/dump/'.$scanned_order_id.'.pdf';
    if(!file_exists($writeto)){
        //This is the file where we save the information
        file_put_contents($writeto, file_get_contents($url));

        shell_exec('print.bat ' . $scanned_order_id);

        echo '<div style="margin-top:100px" class="alert alert-success"><strong>Success</strong> AWB Generated and Sent to Printing...</div>';
    }else{
        file_put_contents($writeto, file_get_contents($url));
        
        shell_exec('print.bat ' . $scanned_order_id);

        echo '<div style="margin-top:100px" class="alert alert-warning"><strong>Warning:</strong> AWB Already Previously Printed!</div>';
    }
}

$scanned_order_id = "";
if (!empty($_POST)){
    $scanned_order_id = $_POST['scanned_order_id'];
    $imei = '';
    if ($_POST['imei_no']) {
        foreach ($_POST['imei_no'] as $item) {
            $imei .= '&imei_arr[]='.$item;
        }
    }
}

if($scanned_order_id){
    try {
        $url = file_get_contents('https://sync.synagie.com/btfl_simplypost_lazada_scanner/lazada_imei.php?scanned_order_id='.$scanned_order_id.$imei);
        if (strpos($url, 'http') !== false){
            //echo "This is the url to pdf:".$url;
            print_label($url,$scanned_order_id);
        }else{
            echo '<div class="alert alert-danger"><strong>Error:</strong> '.$url.'</div>';
        }
    } catch (Exception $e) {
        error_log('Caught exception: ',  $e->getMessage(), "\n");
        echo '<div class="alert alert-danger"><strong>Error:</strong> Server Error!</div>';
    }
}
?>
<h1 style="margin-top: 100px;">Lazada Simplypost IMEI Scanner</h1>
<p>Please only scan the items *AFTER* QC <is></is> completed.<br>And only scan Simplypost Lazada Orders.</p>
<form method="post" id="imei_form">
    <div class="form-group main">
        <button type="button" id="add" style="width: 100%;height: 80px;font-size: 25px;" class="btn btn-warning">Add More IMEI</button>
        <h3 for="scanned_order_id">Scan Order Number</h3>
        <input style="width:100%;float:left;margin-bottom: 25px" type="text" name="scanned_order_id" placeholder="Order Number" class="form-control" id="scanned_order_id"/>
        <h3 for="scanned_order_id">Scan IMEI Number</h3>
        <input style="width:100%;float:left;margin-bottom: 25px" type="text" name="imei_no[]" placeholder="IMEI Number" required class="form-control item"/>
    </div>
  <button type="submit" id="submit" class="btn btn-primary">Retrieve &amp; Print AWB</button>
</form>
<script>
$(document).ready(function(){
    $('#scanned_order_id').focus();

    $('#number').change(function(){
        $('#scanned_order_id').focus();

    });

    $('#plus').click(function(){
        $('#number').val(parseInt($('#number').val()) + 1);
        $('#scanned_order_id').focus();
    });

    $('#minus').click(function(){
        if(parseInt($('#number').val()) > 1){
            $('#number').val(parseInt($('#number').val()) - 1);
            $('#scanned_order_id').focus();
        }
    });

    $('#add').click(function(e){
        e.preventDefault();
        $('.main').append('<div class="input-group" style="margin-bottom: 25px"><input style="width:100%;float:left;" type="text" name="imei_no[]" placeholder="IMEI Number" required class="form-control item"/><span class="input-group-addon"><a href="#" class="del-row">Remove</a></span></div>');
        $('.del-row').click(function(e){
            e.preventDefault();
            ($(this).parent().parent()).remove();
        })
    })

});
</script>
</body>
</html>


