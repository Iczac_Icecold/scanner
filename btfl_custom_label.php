
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<title>Custom Label Scanner</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style>
html{
    text-align:center;
}
body{
    text-align:center;
}
form{
    max-width:500px;
    margin:0 auto;
    margin-top:200px;

}
form input{
    font-size:50px;
}
.form-control{
    height:auto;
    font-size:50px;
}
#number{
    pointer-events:none;
}
#wrap a{
    display:block;
    font-size:30px;
    display: block;
    border-radius: 25px;
    font-size: 15px;
    padding: 10px;
    border: 1px solid gray;
}
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

</head>

<body>
<form action="" method="post">
<?php
ini_set("log_errors", 1);
error_reporting(E_ALL); 
ini_set("error_log", "./logs/".date('Y-m-d')."shipment_create.log");

function print_label($url,$scanned_order_id){
    $writeto = dirname(__FILE__) . '/dump/'.$scanned_order_id.'.pdf';
    if(!file_exists($writeto)){
        //This is the file where we save the information
        file_put_contents($writeto, file_get_contents($url));

        shell_exec('print.bat ' . $scanned_order_id);

        echo '<div class="alert alert-success"><strong>Success</strong> AWB Generated and Sent to Printing...</div>';
    }else{
        file_put_contents($writeto, file_get_contents($url));
        
        shell_exec('print.bat ' . $scanned_order_id);

        echo '<div class="alert alert-warning"><strong>Warning:</strong> AWB Already Previously Printed!</div>';
    }
}

$scanned_order_id = "";
if (!empty($_POST)){
    $scanned_order_id = $_POST['scanned_order_id'];
}
if($scanned_order_id){
    try {

        $url = file_get_contents('http://sync.synagie.com/btfl_custom_scanner/receiver.php?scanned_order_id='.$scanned_order_id);
        
        if (strpos($url, 'http') !== false){
            //echo "This is the url to pdf:".$url;
            print_label($url,$scanned_order_id);
        }else{
            echo '<div class="alert alert-danger"><strong>Error:</strong> '.$url.'</div>';
        }
    } catch (Exception $e) {
        error_log('Caught exception: ',  $e->getMessage(), "\n");
        echo '<div class="alert alert-danger"><strong>Error:</strong> Server Error!</div>';
    }
}
?>
<h1>Custom Label Scanner</h1>
<p>Please only scan the items *AFTER* QC is completed.</p>
<form method="post" name="myform">
  <div class="form-group">
    <label for="pwd">Scan Order Number</label>
    <input style="width:100%;float:left;" type="text" name="scanned_order_id" class="form-control" id="scanned_order_id"/>
    <!--<input style="width:15%;float:left;text-align:center;" type="text" name="number" class="form-control" id="number" value="1"/>
    <div id="wrap" style="width:10%;display:inline-block;float:left;">
        <a id="plus" href="#">
            <span class="glyphicon glyphicon-plus"></span>
        </a>
        <a id="minus" href="#">
            <span class="glyphicon glyphicon-minus"></span>
        </a>
    </div>-->
  </div>
  <button type="submit" class="btn btn-default">Retrieve &amp; Print AWB</button>
</form>
<script>
$(document).ready(function(){
    $('#scanned_order_id').focus();
    $('#number').change(function(){
        $('#scanned_order_id').focus();

    });
    $('#plus').click(function(){
        $('#number').val(parseInt($('#number').val()) + 1);
        $('#scanned_order_id').focus();
    });
    $('#minus').click(function(){
        if(parseInt($('#number').val()) > 1){
            $('#number').val(parseInt($('#number').val()) - 1);
            $('#scanned_order_id').focus();
        }
    });
});
</script>
</body>
</html>


