<?php

function get_channel_order_Id($order_id) {
    $curl = curl_init();

    $url = "https://api.app.synagie.com/v1/order/details/" . $order_id . "/";
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'api-key: 0c3d93d1424abe44f6a6bda6c8d32034cc66cf36'
    ));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curl);
    $response = json_decode($response, true);

    return $response['response']['channel_order_id'];
}

get_channel_order_Id('BTFLDE-321516');
